<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LinksController@index')->name('index');

Route::post('/shorten', 'LinksController@shorten')->name('shorten');

Route::get('/short/{link}', 'LinksController@shortlink')->name('short');

Route::get('/list', 'LinksController@list')->name('links_list');

Route::get('/info/{link}', 'LinksController@info')->name('link_info');

Route::delete('/destroy/{link}', 'LinksController@destroy')->name('link_destroy');

