<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\URL;
use App\Link;
use App\Redirect;

class LinksController extends Controller
{
    public function index(){

        return view('index');
    }

    public function shorten(Request $request)
    {
        if ($request['original_link']) {
            $link = new Link();
            if ($request['custom_link']) {
                $link->original_link = $request['original_link'];
                $link->short_link = URL::to('/') . '/' . 'short/' . $request['custom_link'];
                $link->token = $request['custom_link'];
            } else {
                $random_token = Str::random(8);
                $link->original_link = $request['original_link'];
                $link->short_link = URL::to('/') . '/' . 'short/' . $random_token;
                $link->token = $random_token;
            }
            if ($request['expired']){

                $link->expired = time() + 3600;
            }
            $link->save();
            $data = ["short_link" => $link->short_link];

            return view('short_link', $data);
        }

        return redirect('/')->withErrors(['error' => 'Input is empty']);
    }


    public function shortlink($link)
    {
        $short_link = URL::to('/') . '/short/' . $link;
        $res_link = Link::where('short_link', $short_link)->first();
        $user_ip = $_SERVER['REMOTE_ADDR'];

        if ($res_link['original_link']){

            if ($res_link['expired'] && $res_link['expired'] < time()) {

                return redirect('/')->withErrors(['error' => 'Link has expired']);
            }

            $redirect = new Redirect();
            $redirect->user_agent = $_SERVER['HTTP_USER_AGENT'];
            $redirect->link_id = $res_link->id;
            $redirect->user_ip = $user_ip;
            $redirect->save();

            return redirect($res_link['original_link']);
        } else {

            return redirect('/')->withErrors(['error' => 'Link does not exist']);
        }
    }


    public function info(Link $link)
    {
        if ($link){

            $data = ['link' => $link ];

            return view('link_info', $data);

        } else {

            return redirect('links_list')->withErrors(['error' => 'Link is not exist']);
        }
    }


    public function list()
    {
        $links = Link::all();
        $data = ['links' => $links];

        return view('links_list', $data);
    }


    public function destroy(Request $request, Link $link)
    {
        if ($request->isMethod('DELETE')) {

            $link->delete();

            return redirect('/list')->with('status', 'The Link was deleted');
        }

        return redirect('/list')->withErrors(['error' => 'Link wasn\'t delete']);
    }
}
