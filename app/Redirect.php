<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Redirect extends Model
{
    protected $fillable = [
        'link_id',
        'date',
        'user_agent',
    ];


    public function link()
    {
        return $this->belongsTo('App\Link');
    }

}
