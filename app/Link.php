<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $fillable = [
        'original_link',
        'short_link',
        'user_ip',
        'token',
        'expired',
    ];

    public function redirects()
    {
        return $this->hasMany('App\Redirect', 'link_id');
    }
}
