@extends('basic')

@section('links_list')

    <div class="container-fluid">

        <div class="text-center" style="margin-top: 50px">
            <a class="btn btn-primary" href="{{ route('index') }}">Back to main Page</a>
        </div>

    </div>

    <div class="container-fluid">

        <div class="text-center" style="margin-top: 50px"><h5>LINKS LIST</h5></div>

        @if ($links)
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Original Link</th>
                    <th>Short Link</th>
                    <th>Redirect</th>
                    <th>Link INFO</th>
                    <th>Delete</th>
                </tr>
                </thead>
                @foreach ($links as $link)
                    <tr>
                        <td>{{ $link->id}}</td>
                        <td>
                            <a href="{{ $link->original_link }}" target="_blank">{{ $link->original_link}}</a>
                        </td>
                        <td>{{ $link->short_link }}</td>
                        <td>
                            <a class="btn btn-primary" href="{{ route('short', $link->token)}}" target="_blank">Redirect</a>
                        </td>
                        <td>
                            <a class="btn btn-primary" href="{{ route('link_info', $link)}}">Link Info</a>
                        </td>
                        <td>
                            {{--Delete--}}
                            {!! Form::open(['url' => route('link_destroy', $link), 'class'=>'form-horizontal', 'method'=>'DELETE']) !!}
                            {{ method_field('DELETE') }}
                            {!! Form::button('Delete', ['class'=>'btn btn-danger', 'type'=>'submit'])!!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            </table>
        @else
            <div class="text-center"><h4>NO Fields List</h4></div>
        @endif
    </div>

@endsection
