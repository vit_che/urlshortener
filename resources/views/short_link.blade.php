@extends('basic')

@section('short_link')

    <div class="flex-center position-ref full-height">

        <div class="text-center">

            <div class="text-center">
                <a class="btn btn-primary" href="{{ route('index') }}">Back to main Page</a>
            </div>

            <div class="title m-b-md">
                URL Shortener
            </div>

            <div class="app">

                @if($short_link)
                    <h6>REDIRECT TO</h6>
                    <div class="text-center">
                        <a href="{{ $short_link }}" target="_blank">{{ $short_link }}</a>
                    </div>

                @else
                    <div class="text-center"><h4>no short link</h4></div>
                @endif

            </div>
        </div>
    </div>

@endsection
