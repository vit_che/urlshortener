@extends('basic')

@section('link_info')

    <div class="container-fluid">

        <div class="text-center" style="margin-top: 50px">
                <a class="btn btn-primary" href="{{ route('index') }}">Back to main Page</a>
                <a class="btn btn-secondary" href="{{ route('links_list') }}">Back to Links List</a>
        </div>

        <div class="text-center" style="margin-top: 50px">
            <h4>Link Info</h4>
        </div>

            @if ($link)
                <table class="table table-hover table-striped">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Original link</th>
                        <th>Shortered link</th>
                        <th>Redirect Count</th>
                        <th>Expired</th>
                        <th>Redirect</th>
                    </tr>
                    </thead>
                    <tr>
                        <td>{{ $link->id}}</td>
                        <td>{{ $link->original_link }}</td>
                        <td>{{ $link->short_link }}</td>
                        <td>{{ count($link->redirects) }}</td>
                        <td>
                            @if( $link->expired != null)
                                @if($link->expired > time() )
                                    {{ $link->expired }}
                                @else
                                    {{ 'link has expired' }}
                                @endif
                             @else
                                {{ 'NULL' }}
                             @endif
                        </td>
                        <td>
                            <a class="btn btn-primary" href="{{ route('short', $link->token)}}" target="_blank">Redirect</a>
                        </td>
                    </tr>
                </table>
            @else
                <div class="text-center"><h4>NO Link</h4></div>
            @endif
        </div>

        <div class="container">
            <div id="columnchart_values" style="width: 900px; height: 300px;"></div>
        </div>

    </div>

@endsection

@push('scripts')

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load("current", {packages:['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ["Counrty", "Quantity", { role: "style" } ],
                ["Ukraine", 8, "#b87333"],
                ["Spain", 10, "silver"],
                ["Poland", 19, "gold"],
                ["France", 21, "color: #e5e4e2"]
            ]);

            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1,
                { calc: "stringify",
                    sourceColumn: 1,
                    type: "string",
                    role: "annotation" },
                2]);

            var options = {
                title: "Counts redirects",
                width: 600,
                height: 400,
                bar: {groupWidth: "95%"},
                legend: { position: "none" },
            };
            var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
            chart.draw(view, options);
        }
    </script>

@endpush

