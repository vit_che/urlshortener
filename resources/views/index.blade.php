@extends('basic')

@section('index')

    <div class="container-fluid">

        <div class="text-center" style="margin-top: 50px">
            <a class="btn btn-primary" href="{{ route('links_list') }}">LINKs List</a>
        </div>

        <div class="flex-center position-ref full-height">

            <div class="content">
                <div class="title m-b-md">
                    URL Shortener
                </div>

                {!! Form::open(['url'=>route('shorten'),'class'=>'form-horizontal','method'=>'POST','enctype'=>'multipart/form-data']) !!}

                {{--LINK--}}
                <div class="form-group">

                    <div class="row">
                        <div class="col-sm-6">
                            {!! Form::label('original_link', 'Original Link', ['class' => 'control-label']) !!}
                        </div>
                        <div class="col-sm-6">
                            {!! Form::url('original_link', old('original_link'), ['class' => 'form-control',
                                                    'placeholder'=>'Input a link to shorten']) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#custom_link" aria-expanded="true" aria-controls="collapseExample">
                                Custom Link
                            </button>
                        </div>
                        <div class="col-sm-6 collapse" id="custom_link">
                            {!! Form::text('custom_link', old('custom_link'), ['class' => 'form-control',
                                                    'placeholder'=>'Input a custom link to shorten']) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            {!! Form::label('expired', 'Set Expire Time 1 Hour', ['class' => 'control-label']) !!}
                        </div>
                        <div class="col-sm-6">
                            {!! Form::checkbox('expired') !!}
                        </div>
                    </div>

                </div>

                {{--SHORTEN BUTTON--}}
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-offset-1 col-sm-8">
                            {!! Form::button('Shorten', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
                        </div>
                    </div>
                </div>

                {!! Form::close() !!}

            </div>
        </div>

    </div>

@endsection

